package seleniumapitests;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sasikala on 11/09/2014.
 * Take Screenshot of Window using the TakeScreenShot Interface
 */
public class ScreenShot {
    WebDriver driver = new FirefoxDriver();

    @Before
    public void set_up(){

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }
    @Test

    public void Screen_shotTest() throws IOException, InterruptedException {

        // go to BBC weather website and look for weather in "City of Westminster"

        driver.get("http://www.bbc.co.uk/weather/");
        driver.findElement(By.id("locator-form-search")).sendKeys("City of Westminster");
        driver.findElement(By.id("locator-form-submit")).click();

        // Take Screenshot of the weather report

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(scrFile, new File("C:\\Users\\Sasikala\\Documents\\Testing\\bccweather.jpg"));

        Thread.sleep(1000);
        assertEquals( "CITY OF WESTMINSTER", driver.findElement(By.xpath("//*[@id='blq-content']/div[1]/h1/span")).getText());
    }

    @After

        public void wrap_up(){

            driver.quit();
        }
}
