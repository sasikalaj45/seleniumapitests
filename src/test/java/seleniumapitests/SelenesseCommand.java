package seleniumapitests;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sasikala on 23/03/2015.
 * Test running Selenium 1 commands using DefaultSelenium class
 * Make sure you start the Selenium server in the local host at port 4444 before running this script
 */
public class SelenesseCommand {

    Selenium sel = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe",
            "http://www.google.com");

    @Before
    public void set_up(){

        sel.start();

    }

    @Test
    public void SeleniumCommand() throws InterruptedException {

        sel.open("http://www.google.com.au/");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        sel.type("name=q", "Selenium");
        sel.click("name=btnG");

    }
    @After

    public void wrap_up(){

        sel.close();
        sel.stop();

    }
}
