package seleniumapitests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sasikala on 05/09/2014.
 * Get the CSS attributes of the Webelemnts using the getCSS API in the WebElement Class
 */
public class GetCssvalue {

   WebDriver driver = new FirefoxDriver();

   @Before
   public void set_up(){
       driver.manage().window().maximize();
       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
   }
   @Test
    public void getcss_value() throws InterruptedException {

       driver.get("http://demo.smartstore.com/frontend/en/");

       // Search for a product and go to product page

       WebElement quickSearch = driver.findElement(By.id("quicksearch"));
       quickSearch.sendKeys("samsung galaxy");
       WebElement searchIcon = driver.findElement((By.xpath("//*[@id='shopbar']/div/form/div/button")));
       searchIcon.click();

       driver.findElement(By.xpath(".//*[@id='content-center']/div/div[2]/form/div[4]/div[1]/div/div[1]/div[1]/article/div[2]/h3/a")).click();

       Thread.sleep(1000);

       // find for add-to-cart element and get its CSS value

       WebElement add_to_cart = driver.findElement(By.xpath("//*[@id='AddToCart']/div[2]/div/a"));

       System.out.println("size" + add_to_cart.getCssValue("font-size"));
       System.out.println("color" + add_to_cart.getCssValue("background-color"));
       System.out.println("type" +add_to_cart.getCssValue("font-family"));

   }

       @After
       public void wrap_up () {
           driver.quit();
       }
   }

