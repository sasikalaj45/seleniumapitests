package seleniumapitests;


import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;
/**
 * Created by Sasikala on 05/09/2014.
 * Run Selenium 1 commands along with the Webdriver commands using the WebDriverBackedSelenium class
 */
public class WebdriverBackedSelenium {
   WebDriver driver = new FirefoxDriver();
   Selenium sel;

    @Before
    public void set_up(){

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }
    @Test
    public  void WebdriverBacked() throws InterruptedException {

            String baseUrl = "http://www.google.com.au/";
            sel = new WebDriverBackedSelenium(driver, baseUrl);

            sel.open("http://www.google.com.au/");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sel.type("name=q", "Packt Publishing");
            sel.click("name=btnG");
            sel.click("//*[@id='rso']/li/div/div/h3/a");
            Thread.sleep(1000);
    }

    @After

    public void wrap_up(){

        sel.close();
        sel.stop();
        driver.quit();
    }
}



