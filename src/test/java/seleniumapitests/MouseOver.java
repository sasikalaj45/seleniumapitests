package seleniumapitests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Sasikala on 23/03/2015.
 * Simulate mouseover user interaction using Selenium Webdriver APIs in Actions class.
 */
public class MouseOver {

    WebDriver driver = new FirefoxDriver();

    @Before
    public void set_up(){

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void MouseHover() {

        // login into nopcommerce demo site

        driver.get("http://admin-demo.nopcommerce.com/");
        driver.findElements(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys("admin@yourStore.com");
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@value='Log in']")).click();

        // mousehover for Catalog menu
        Actions catalog_menu = new Actions(driver) ;
        WebElement cm = driver.findElement(By.xpath("//*[@id='admin-menu']/li[2]/span"));
        catalog_menu.moveToElement(cm).build().perform();

        // mousehover and click for Products submenu
        WebElement product = driver.findElement(By.xpath("//*[@id='admin-menu']/li[2]/div/ul/li[3]/span"));
        catalog_menu.moveToElement(product).click().build().perform();

        // mousehover and click() for Manage products submenu
        WebElement manage_product = driver.findElement(By.xpath(".//*[@id='admin-menu']/li[2]/div/ul/li[3]/div/ul/li[1]/a"));
        catalog_menu.moveToElement(manage_product).click().build().perform();

        // assert Manage products page is opened
        assertEquals("Manage Products",driver.findElement(By.xpath("html/body/div[2]/div/div[5]/div/form[1]/div/div[1]")).getText());

    }

    @After

    public void wrap_up(){

        driver.quit();
    }

}
