package seleniumapitests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sasikala on 05/09/2014.
 * Simuate drag and drop user action. Move webelement on the screen by setting horizontal and vertical offsets
 */
public class ScrollWebElement {

    WebDriver fdriver;

    @Test
    public void scroll_Bar_Test ()throws InterruptedException
    {
        FirefoxProfile fprofile = new FirefoxProfile();
        fprofile.setEnableNativeEvents(true);

        fdriver = new FirefoxDriver(fprofile);

        fdriver.manage().window().maximize();
        fdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        fdriver.get("http://jqueryui.com/draggable");
        Thread.sleep(2000);

        fdriver.switchTo().frame(fdriver.findElement(By.xpath("//*[@class='demo-frame']")));

        WebElement element= fdriver.findElement(By.xpath("//html/body/div/p"));

        Actions action = new Actions(fdriver);

        action.dragAndDropBy(element, 50, 50).build().perform();

        Thread.sleep(2000);


    }

    @After

    public void wrap_up(){

        fdriver.quit();
    }

}
