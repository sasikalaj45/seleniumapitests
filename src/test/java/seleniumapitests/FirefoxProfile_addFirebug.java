package seleniumapitests;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/*
 * Created by Sasikala on 05/09/2014.
 * Test to add Firebug extension to the firefox profile when the browser opens through the Selenium webdriver
 */
public class FirefoxProfile_addFirebug {

    public WebDriver webDriver;
    public String baseUrl = "https://www.amazon.co.uk/";

    FirefoxProfile profile = new FirefoxProfile();

    FirefoxBinary binary = new FirefoxBinary(new File("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"));


    @Before
    public void setUp() throws Exception {

        profile.addExtension(new File("C:\\Users\\Sasikala\\Documents\\Softwares\\firebug-2.0.1-fx.xpi"));

        webDriver = new FirefoxDriver(binary, profile);

        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get(baseUrl + "/");
    }

    @Test
    public void FireFoxFireBugExtension() throws InterruptedException, IOException {

        // Take Screen shot of the window that opens with the firefox extension
        Thread.sleep(1000);
        File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(scrFile, new File("C:\\Users\\Sasikala\\Documents\\Testing\\firebug.jpg"));

    }

    @After
    public void tearDown() throws Exception {
        webDriver.quit();
    }
}
