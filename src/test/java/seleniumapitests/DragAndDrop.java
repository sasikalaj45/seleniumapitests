package seleniumapitests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
/**
 * Created by Sasikala on 05/09/2014.
 * Test to drag and drop a webelement based on from and to elements location
 */
public class DragAndDrop {

   WebDriver driver = new FirefoxDriver();

   @Before
   public void set_up(){

       driver.manage().window().maximize();
       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

   }

   @Test
   public  void dragAndDrop() throws InterruptedException {

       String URL = "http://www.dhtmlx.com/docs/products/dhtmlxTree/index.shtml";
       driver.get(URL);

       WebElement From = driver.findElement(By.xpath(".//*[@id='treebox1']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span"));

       String from_book = From.getText();

       WebElement To =  driver.findElement(By.xpath(".//*[@id='treebox2']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span"));

       (new Actions(driver)).dragAndDrop(From, To).build().perform();

       // To assert that the element has been moved to the desired location

       List<WebElement> table_elements = driver.findElements(By.xpath(".//*[@id='treebox2']/div/table/tbody/tr[2]/td[2]/table/tbody"));

       boolean element_found = false;

       for (WebElement a : table_elements) {

            if ( a.getText().trim().contains(from_book.trim())) {

                element_found = true;
            }
       }
       assertTrue(element_found);

       Thread.sleep(1000);
   }
    @After

    public void wrap_up(){

        driver.quit();
    }

}

