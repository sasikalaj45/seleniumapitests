package seleniumapitests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by Sasikala on 05/09/2014.
 * Change the browser setting to set the language preference as "English"
 */
public class Internationlisation {


    @Test
    public void internationalization_test() throws InterruptedException
    {

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("intl.accept_languages","en");
        WebDriver driver = new FirefoxDriver(profile);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://en.wikipedia.org");
        driver.findElement(By.id("searchInput")).sendKeys("JUnit");
        driver.findElement(By.id("searchButton")).click();

        driver.findElement(By.xpath(".//*[@id='mw-content-text']/table/tbody/tr[7]/td/span/a")).click();

        assertTrue(driver.getCurrentUrl().contains("junit.org"));

        Thread.sleep(2000);
        driver.quit();
    }

}
